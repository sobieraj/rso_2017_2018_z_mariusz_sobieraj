using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfProjectServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IMyBookService
    {
        List<Book> bookList = new List<Book>();

        public Service1()
        {
            initList();
            Console.WriteLine("Kostruktor");
        }

        public void initList()
        {
            for(int i = 0; i < 5; i++)
                {
                    Book b = new Book();
                    b.BookID = 1+i;
                    b.BookName = "Nasza szkapa część : " + i ;
                    b.Borrowed = true;
                    b.ReaderID = 5+i;
                // data czas na sztywno rok ,miesiac  , dzień, godzina , min, sek
                    b.DTBorrowedBook = new DateTime(2018, 5 + i, 1 + i, 8, 30, 52);
                    bookList.Add(b);
                    Console.WriteLine("initFunction");
                }
        }
        ////////////////////////////////////////////////////////////////////////////////
        //public string GetData(int value)
       // {
        //    return string.Format("You entered: {0}", value);
       // }

       // public CompositeType GetDataUsingDataContract(CompositeType composite)
       // {
         //   if (composite == null)
           // {
            //    throw new ArgumentNullException("composite");
            //}
           // if (composite.BoolValue)
           // {
            //    composite.StringValue += "Suffix";
          //  }
           // return composite;
       // }
        /////////////////////////////////////////////////////////////////////////////////
        public List<Book> GetBookList()
        {
            return bookList;
        }

        public List<Book> GetBorrowedBooks()
        {
            List<Book> borrowedList = new List<Book>();
            for(int i = 0; i < bookList.Count; i++)
            {
                if(bookList[i].Borrowed == true)
                {
                    borrowedList.Add(bookList[i]);
                }
            }
            return borrowedList;
        }

        public List<Book> GetBorrowedBooksList(int userID)
        {
            List<Book> borrowedUserList = new List<Book>();
            try
            {
                for (int i = 0; i < bookList.Count; i++)
                {
                    if (bookList[i].ReaderID == userID)
                    {
                        borrowedUserList.Add(bookList[i]);
                    }
                }
                return borrowedUserList;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                UserIdOoutOfRangeFault fault = new UserIdOoutOfRangeFault();

                fault.Result = false;
                fault.Message = ex.Message;
                fault.Description = "User Id out of range. GetBorrowedBookList(UserID).";
                throw new FaultException<UserIdOoutOfRangeFault>(fault);
            }

        }

        public string GetBookInfo(int bookID)
        {
            try
            {
                return bookList[bookID].BookName;
            }
            catch(ArgumentOutOfRangeException ex)
            {
                BookIdOoutOfRangeFault fault = new BookIdOoutOfRangeFault();

                fault.Result = false;
                fault.Message = ex.Message;
                fault.Description = "Book Id out of range. GetBookInfo(BookID).";
                throw new FaultException<BookIdOoutOfRangeFault>(fault);
            }
        }

        public bool GetBookStatus(int bookID)
        {
            try
            {
                return bookList[bookID].Borrowed;
            }
            catch (ArgumentOutOfRangeException ex)
            {
                BookIdOoutOfRangeFault fault = new BookIdOoutOfRangeFault();

                fault.Result = false;
                fault.Message = ex.Message;
                fault.Description = "Book Id out of range. GetBookStatus(BookID).";
                throw new FaultException<BookIdOoutOfRangeFault>(fault);
            }
        }
    }
}
