#include <Ice/Ice.h>

#include <Calc.h>



using namespace std;

using namespace Demo;

 

int

main(int argc, char* argv[])

{

    int status = 0;

    Ice::CommunicatorPtr ic;

    try {

        ic = Ice::initialize(argc, argv);

        Ice::ObjectPrx base = ic->stringToProxy("SimpleCalc:default -p 10000");

        CalcPrx calc = CalcPrx::checkedCast(base);

        if (!calc)

            throw "Invalid proxy";
                        //  double x;

                        //cout<<endl<<"podaj Liczbę"<<endl;

                         //cin>>x;

             // double value = x ;

		double value = 4;



		cout << "sqrt("<< value << ") result:" << calc->sqrt(value) << endl;;

        

    } catch (const Ice::Exception& ex) {

        cerr << ex << endl;

        status = 1;

    } catch (const char* msg) {

        cerr << msg << endl;

        status = 1;

    }

    if (ic)

        ic->destroy();

    return status;

}
