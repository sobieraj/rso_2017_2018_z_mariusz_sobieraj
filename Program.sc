using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfProjectClient
{
    class Program
    {
        static void Main(string[] args)
        {
            BookServiceReference.MyBookServiceClient mbsc = new BookServiceReference.MyBookServiceClient();

            try
            {
                mbsc.Open();
                // liste ksiazek
                Console.WriteLine("Lista ksiazek: ");
               // Console.WriteLine(mbsc.GetBookList());
                for(int i = 0; i < mbsc.GetBookList().Count(); i++)
                {
                    Console.WriteLine(mbsc.GetBookList().ElementAt(i).BookName);
                   


                }


                //pożyczone
                Console.WriteLine("Ksiazki pozyczone: ");
                //Console.WriteLine(mbsc.GetBorrowedBooksList(5));
                Console.WriteLine(mbsc.GetBorrowedBooksList(5).ElementAt(0).BookName);
                Console.WriteLine(mbsc.GetBorrowedBooksList(5).ElementAt(0).DTBorrowedBook);

                //Pobiera info
                Console.WriteLine("Tytuł ksiazki 2: ");
                Console.WriteLine(mbsc.GetBookInfo(2));

                //Status ksiązki OK
                Console.WriteLine("Status ksiazki 1: ");
                Console.WriteLine(mbsc.GetBookStatus(1));

                //zamyka po wczytaniu dowolnego klawisza
                Console.ReadKey();
                mbsc.Close();
            }
            
            catch (FaultException<BookServiceReference.BookIdOoutOfRangeFault> e)
            {
                Console.WriteLine("Message: {0}, Description: {1}");
                Console.WriteLine(e.Message);
                Console.ReadKey();
                mbsc.Abort();
            }

            catch (CommunicationException e)
            {
                Console.WriteLine("Problem z komunikacją z serwerem " );
                Console.ReadKey();
                mbsc.Abort();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                mbsc.Abort();
            }

        }
    }
}
