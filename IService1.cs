using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfProjectServiceLibrary
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMyBookService" in both code and config file together.
    [ServiceContract]
    public interface IMyBookService
    {
       // [OperationContract]
       // string GetData(int value);

      //  [OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        List<Book> GetBookList();

        [OperationContract]
        List<Book> GetBorrowedBooks();

        [OperationContract]
        [FaultContract(typeof(UserIdOoutOfRangeFault))]
        List<Book> GetBorrowedBooksList(int userID);

        [OperationContract]
        [FaultContract(typeof(BookIdOoutOfRangeFault))]
        string GetBookInfo(int bookID);

        [OperationContract]
        [FaultContract(typeof(BookIdOoutOfRangeFault))]
        bool GetBookStatus(int bookID);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "WcfProjectServiceLibrary.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        //string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

      //  [DataMember]
      //  public string StringValue
       // {
         //   get { return stringValue; }
         //   set { stringValue = value; }
       // }
    }
    [DataContract]
    public class Book
    {
        int bookID = 0;
        string bookName = "";
        bool borrowed = false;
        int readerID = 0;
        DateTime dtBorrowedBook;
        DateTime dtReturnedBook;

        [DataMember]
        public int BookID
        {
            get { return bookID; }
            set { bookID = value; }
        }

        [DataMember]
        public string BookName
        {
            get { return bookName; }
            set { bookName = value; }
        }

        [DataMember]
        public bool Borrowed
        {
            get { return borrowed; }
            set { borrowed = value; }
        }

        [DataMember]
        public int ReaderID
        {
            get { return readerID; }
            set { readerID = value; }
        }

        [DataMember]
        public DateTime DTBorrowedBook
        {
            get { return dtBorrowedBook; }
            set { dtBorrowedBook = value; }
        }

        [DataMember]
        public DateTime DTReturnedBook
        {
            get { return dtReturnedBook; }
            set { dtReturnedBook = value; }
        }

    }

    [DataContract]
    public class User
    {
        int userID = 0;
        string userName = "";

        [DataMember]
        public int UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }
    }

    [DataContract]
    public class BookIdOoutOfRangeFault
    {
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class UserIdOoutOfRangeFault
    {
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
